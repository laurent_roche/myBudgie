#!/bin/bash
# Script to install applications after installation of elementary 
# (fork of elementary-postinstall.sh by Devil505 https://github.com/Devil505/elementaryos-postinstall)
#
# Applications installed and command ran:
#(cf. http://computing.travellingfroggy.info/fr/article285/mon-install-de-elementary-os-freya)
#
# https://bitbucket.org/ComputingFroggy/myeos
# (c) LRO - oct 2015


MY_DIR=$(dirname $0)
. $MY_DIR/mc_postinst_common.sh

CHK_REP=$(zenity --entry --title="$BGN_TITLE" --height 200 --width 500 --text "$BGN_TEXT" --entry-text="$BGN_DEF" "$BGN_CHECKED" "$BGN_UNCHECKED")
if [ $? -ne 0 ] ; then
	exit
fi
FN_FINAL_MSG=$(mktemp)
echo "$MSG_END_TEXT" > $FN_FINAL_MSG

#Post-Install List commands
GUI=$(zenity --list --checklist --height 800 --width 900 \
	--title="$MSG_ZEN_TITLE" --text="$MSG_ZEN_TEXT" \
	--column="$MSG_ZEN_CHECK" --column="$MSG_ZEN_ACTION" --column="$MSG_ZEN_DESCRIPTION"  \
	$(chkDef "TRUE") "$CA_PARTNER" "$CD_PARTNER" \
	$(chkDef "TRUE") "$CA_UPGRADE" "$CD_UPGRADE" \
	$(chkDef "TRUE") "$CA_ZRAM" "$CD_ZRAM" \
	$(chkDef "TRUE") "$CA_JAVA" "$CD_JAVA" \
	FALSE "$SCT_INTERNET" "______________________" \
	$(chkDef "TRUE") "$CA_PIDGIN" "$CD_PIDGIN" \
	$(chkDef "TRUE") "$CA_SKYPE" "$CD_SKYPE" \
	$(chkDef "FALSE") "$CA_SLACK" "$CD_SLACK" \
	$(chkDef "FALSE") "$CA_JITSI" "$CD_JITSI" \
	$(chkDef "FALSE") "$CA_TEAMS" "$CD_TEAMS" \
	$(chkDef "FALSE") "$CA_SIGNAL" "$CD_SIGNAL" \
	FALSE "$SCT_OFFICE" "______________________" \
	$(chkDef "TRUE") "$CA_LO_PPA" "$CD_LO_PPA" \
	$(chkDef "TRUE") "$CA_EXTRAFONTS" "$CD_EXTRAFONTS" \
	$(chkDef "TRUE") "$CA_FRDIC" "$CD_FRDIC" \
	$(chkDef "TRUE") "$CA_UKDIC" "$CD_UKDIC" \
	$(chkDef "TRUE") "$CA_XPAD" "$CD_XPAD" \
	$(chkDef "FALSE") "$CA_JOPLIN" "$CD_JOPLIN" \
	$(chkDef "TRUE") "$CA_PDFARRANGER" "$CD_PDFARRANGER" \
	$(chkDef "FALSE") "$CA_UPM" "$CD_UPM" \
	$(chkDef "FALSE") "$CA_KEEPASSXC" "$CD_KEEPASSXC" \
	$(chkDef "FALSE") "$CA_NIXNOTE2" "$CD_NIXNOTE2" \
	$(chkDef "TRUE") "$CA_SAFE_EYES" "$CD_SAFE_EYES" \
	FALSE "$SCT_MULTIMEDIA" "______________________" \
	$(chkDef "TRUE") "$CA_GIMP" "$CD_GIMP" \
	$(chkDef "TRUE") "$CA_VLC" "$CD_VLC" \
	$(chkDef "TRUE") "$CA_AUDACITY" "$CD_AUDACITY" \
	$(chkDef "TRUE") "$CA_RESTRICT_EXTRA" "$CD_RESTRICT_EXTRA" \
	$(chkDef "FALSE") "$CA_BRASERO" "$CD_BRASERO" \
	$(chkDef "TRUE") "$CA_INKSCAPE" "$CD_INKSCAPE" \
	$(chkDef "TRUE") "$CA_SCRIBUS" "$CD_SCRIBUS" \
	$(chkDef "FALSE") "$CA_SUBLIMINAL" "$CD_SUBLIMINAL" \
	$(chkDef "FALSE") "$CA_SUBLIM_NEMO" "$CD_SUBLIM_NEMO" \
	$(chkDef "FALSE") "$CA_SUB_EDIT" "$CD_SUB_EDIT" \
	$(chkDef "FALSE") "$CA_SSR" "$CD_SSR" \
	$(chkDef "FALSE") "$CA_SWEETHOME3D" "$CD_SWEETHOME3D" \
	$(chkDef "FALSE") "$CA_SHOTCUT" "$CD_SHOTCUT" \
	$(chkDef "FALSE") "$CA_YOUTUBE_DL" "$CD_YOUTUBE_DL" \
	$(chkDef "FALSE") "$CA_SCRCPY" "$CD_SCRCPY" \
	$(chkDef "FALSE") "$CA_PEEK" "$CD_PEEK" \
	$(chkDef "FALSE") "$CA_NEMO_IMG_CNVT" "$CD_NEMO_IMG_CNVT" \
	FALSE "$SCT_DEV" "______________________" \
	$(chkDef "TRUE") "$CA_HTOP" "$CD_HTOP" \
	$(chkDef "TRUE") "$CA_GEANY" "$CD_GEANY" \
	$(chkDef "TRUE") "$CA_ARCHIVE" "$CD_ARCHIVE" \
	$(chkDef "TRUE") "$CA_SSHPASS" "$CD_SSHPASS" \
	$(chkDef "FALSE") "$CA_ALACARTE" "$CD_ALACARTE" \
	$(chkDef "FALSE") "$CA_VIRTUALBOX" "$CD_VIRTUALBOX" \
	$(chkDef "FALSE") "$CA_FILEZILLA" "$CD_FILEZILLA" \
	$(chkDef "FALSE") "$CA_REMMINA_PPA" "$CD_REMMINA_PPA" \
	$(chkDef "FALSE") "$CA_ANYDESK" "$CD_ANYDESK" \
	$(chkDef "FALSE") "$CA_MYSQLWB" "$CD_MYSQLWB" \
	$(chkDef "FALSE") "$CA_HG" "$CD_HG" \
	$(chkDef "FALSE") "$CA_GIT" "$CD_GIT" \
	$(chkDef "FALSE") "$CA_MELD" "$CD_MELD" \
	$(chkDef "FALSE") "$CA_RETEXT" "$CD_RETEXT" \
	$(chkDef "FALSE") "$CA_ANSIBLE" "$CD_ANSIBLE" \
	$(chkDef "FALSE") "$CA_RIPGREP" "$CD_RIPGREP" \
	$(chkDef "FALSE") "$CA_FD" "$CD_FD" \
	FALSE "$SCT_UTILS" "______________________" \
	$(chkDef "TRUE") "$CA_DIODON" "$CD_DIODON" \
	$(chkDef "TRUE") "$CA_X11VNC" "$CD_X11VNC" \
	$(chkDef "TRUE") "$CA_SAMBAC" "$CD_SAMBAC" \
	$(chkDef "FALSE") "$CA_GSYSLOG" "$CD_GSYSLOG" \
	$(chkDef "FALSE") "$CA_MULTISYSTEM" "$CD_MULTISYSTEM" \
	$(chkDef "TRUE") "$CA_DROPBOX" "$CD_DROPBOX" \
	$(chkDef "FALSE") "$CA_TLP" "$CD_TLP" \
	$(chkDef "FALSE") "$CA_TLP_THINKPAD" "$CD_TLP_THINKPAD" \
	$(chkDef "FALSE") "$CA_SYNAPTIC" "$CD_SYNAPTIC" \
	FALSE "$SCT_END" "______________________" \
	$(chkDef "TRUE") "$CA_AUTOREMOVE" "$CD_AUTOREMOVE" \
	$(chkDef "TRUE") "$CA_RES_DEP" "$CD_RES_DEP" \
--separator='| ');
# 	$(chkDef "TRUE") "$CA_PLUS" "$CD_PLUS" \

if [ $? -eq 0 ]
then
	f_action_exec "$CA_PARTNER" 'sudo sed -i.bak "/^# deb .*partner/ s/^# //" /etc/apt/sources.list'
	f_action_exec "$CA_UPGRADE" "sudo apt update && sudo apt -y upgrade" "$NS_UPGRADE"
	f_action_install "$CA_ZRAM" zram-config
	f_action_ppa_install "$CA_BFS" ppa:nick-athens30/xenial-ck linux-bb
	f_action_install "$CA_JAVA" openjdk-8-jre-headless

	f_action_install "$CA_PIDGIN" pidgin
	f_action_create_ppa_key_install "$CA_SKYPE" "deb [arch=amd64] https://repo.skype.com/deb stable main" https://repo.skype.com/data/SKYPE-GPG-KEY skypeforlinux
	f_action_get "$CA_SLACK" "https://downloads.slack-edge.com/linux_releases/slack-desktop-4.3.2-amd64.deb"
#	f_action_exec "$CA_JITSI" "wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add - && sudo sh -c \"echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list\" && sudo apt update && sudo apt install -y jitsi"
	f_action_create_ppa_key_install "$CA_JITSI" "deb https://download.jitsi.org stable/" https://download.jitsi.org/jitsi-key.gpg.key jitsi
	f_action_get "$CA_TEAMS" "https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/teams_1.3.00.25560_amd64.deb"
	f_action_exec "$CA_SIGNAL" "wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg && sudo mv signal-desktop-keyring.gpg /usr/share/keyrings/ && echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list && sudo apt update && sudo apt install signal-desktop"

	f_action_ppa_install "$CA_LO_PPA" ppa:libreoffice/ppa libreoffice
	f_action_install "$CA_EXTRAFONTS" "ttf-mscorefonts-installer gsfonts gsfonts-other gsfonts-x11 ttf-mscorefonts-installer t1-xfree86-nonfree fonts-alee ttf-ancient-fonts fonts-arabeyes fonts-arphic-bkai00mp fonts-arphic-bsmi00lp fonts-arphic-gbsn00lp fonts-arphic-gkai00mp fonts-atarismall fonts-dustin fonts-f500 fonts-sil-gentium ttf-georgewilliams ttf-isabella fonts-larabie-deco fonts-larabie-straight fonts-larabie-uncommon ttf-sjfonts ttf-staypuft ttf-summersby fonts-ubuntu-title ttf-xfree86-nonfree xfonts-intl-european xfonts-jmk xfonts-terminus"
	f_action_install "$CA_FRDIC" "myspell-fr-gut wfrench aspell-fr hyphen-fr mythes-fr"
	f_action_install "$CA_UKDIC" hunspell-en-gb
	f_action_install "$CA_XPAD" xpad
	f_action_exec "$CA_JOPLIN" "wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash"
	f_action_install "$CA_PDFARRANGER" pdfarranger
	f_action_get "$CA_UPM" "https://launchpad.net/~adriansmith/+archive/ubuntu/upm/+files/upm_1.14-1~ppa3_all.deb"
	f_action_ppa_install "$CA_KEEPASSXC" ppa:phoerious/keepassxc keepassxc
	f_action_ppa_install "$CA_NIXNOTE2" ppa:nixnote/nixnote2-stable nixnote2
	f_action_ppa_install "$CA_SAFE_EYES" ppa:slgobinath/safeeyes safeeyes

	f_action_install "$CA_GIMP" "gimp gimp-help-fr gimp-data-extras"
#	f_action_ppa_install "$CA_GIMP" ppa:otto-kesselgulasch/gimp "gimp gimp-help-fr gimp-data-extras"
	f_action_install "$CA_VLC" vlc
	f_action_install "$CA_AUDACITY" audacity
	f_action_install "$CA_RESTRICT_EXTRA" "ubuntu-restricted-extras libavcodec-extra mpg321"
	f_action_install "$CA_BRASERO" brasero
	f_action_install "$CA_INKSCAPE" "inkscape aspell-fr"
	f_action_install "$CA_SCRIBUS" "scribus scribus-doc"
	f_action_install "$CA_SUBLIMINAL" subliminal
	f_action_exec "$CA_SUBLIM_NEMO" "wget https://raw.githubusercontent.com/Diaoul/nautilus-subliminal/nemo/install.sh -O - | sudo sh"
	f_action_install "$CA_SUB_EDIT" subtitleeditor
	f_action_ppa_install "$CA_SSR" ppa:maarten-baert/simplescreenrecorder simplescreenrecorder
	f_action_ppa_install "$CA_YOUTUBE_DL" ppa:rvm/smplayer youtube-dl
	f_action_exec "$CA_SWEETHOME3D" "wget http://sourceforge.net/projects/sweethome3d/files/SweetHome3D/SweetHome3D-6.0/SweetHome3D-6.0-linux-x64.tgz/download && tar xzf SweetHome3D-6.0-linux-x64.tgz && sudo mv SweetHome3D-6.0 /opt"
	f_action_exec "$CA_SHOTCUT" "sudo snap install shotcut --classic"
	f_action_ppa_install "$CA_YOUTUBE_DL" ppa:rvm/smplayer youtube-dl
	f_action_install "$CA_SCRCPY" scrcpy
	f_action_install "$CA_PEEK" peek
	f_action_get "$CA_NEMO_IMG_CNVT" "https://ftp5.gwdg.de/pub/linux/debian/mint/packages/pool/backport/n/nemo-image-converter/nemo-image-converter_5.6.0+vera_amd64.deb"
	
	f_action_install "$CA_HTOP" htop
	f_action_install "$CA_GEANY" "geany geany-plugin-*"
#	f_action_install "$CA_GEANY" "geany geany-plugin-addons geany-plugin-miniscript geany-plugin-autoclose geany-plugin-multiterm geany-plugin-automark geany-plugin-numberedbookmarks geany-plugin-codenav geany-plugin-overview geany-plugin-commander geany-plugin-pairtaghighlighter geany-plugin-ctags geany-plugin-pg geany-plugin-debugger geany-plugin-pohelper geany-plugin-defineformat geany-plugin-prettyprinter geany-plugin-devhelp geany-plugin-prj geany-plugin-doc geany-plugin-projectorganizer geany-plugin-extrasel geany-plugin-py geany-plugin-gendoc geany-plugin-scope geany-plugin-geniuspaste geany-plugin-sendmail geany-plugin-git-changebar geany-plugin-shiftcolumn geany-plugin-gproject geany-plugin-spellcheck geany-plugin-insertnum geany-plugin-tableconvert geany-plugin-latex geany-plugin-treebrowser geany-plugin-lineoperations geany-plugin-updatechecker geany-plugin-lipsum geany-plugin-vc geany-plugin-lua geany-plugin-webhelper geany-plugin-macro geany-plugin-xmlsnippets geany-plugin-markdown"
	f_action_install "$CA_SSHPASS" "sshpass"
	f_action_install "$CA_ALACARTE" "alacarte"
	f_action_exec "$CA_VIRTUALBOX" "echo \"deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib\" | sudo tee /etc/apt/sources.list.d/virtualbox.list && wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc -O- | sudo apt-key add - && sudo apt update && sudo apt install -y virtualbox-5.2 && sudo usermod -G vboxusers -a $USER" 
	f_action_install "$CA_ARCHIVE" "unace rar unrar p7zip-rar p7zip-full sharutils uudeview mpack arj cabextract lzip lunzip"
	f_action_install "$CA_FILEZILLA" filezilla
#	f_action_install "$CA_REMMINA" remmina
#	f_action_ppa_install "$CA_REMMINA_PPA" ppa:remmina-ppa-team/remmina-next "remmina remmina-plugin-rdp libfreerdp-plugins-standard"
	f_action_ppa_install "$CA_REMMINA_PPA" ppa:remmina-ppa-team/remmina-next "remmina remmina-plugin-rdp freerdp2-x11 remmina-plugin-nx remmina-plugin-exec remmina-plugin-kwallet remmina-plugin-xdmcp remmina-plugin-www"
#	f_action_get "$CA_ANYDESK" "https://download.anydesk.com/linux/anydesk_5.5.4-1_amd64.deb"
	f_action_create_ppa_key_install "$CA_ANYDESK" "deb http://deb.anydesk.com/ all main" https://keys.anydesk.com/repos/DEB-GPG-KEY anydesk
#	f_action_get "$CA_MYSQLWB" "http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-workbench-community-6.3.7-1ubu1604-amd64.deb"
	f_action_install "$CA_MYSQLWB" mysql-workbench
	f_action_install "$CA_HG" mercurial
	f_action_install "$CA_GIT" git
	f_action_install "$CA_MELD" meld
	f_action_install "$CA_RETEXT" retext
	f_action_ppa_install "$CA_ANSIBLE" ppa:ansible/ansible ansible 
	f_action_exec "$CA_RIPGREP" "sudo snap install ripgrep --classic"
	f_action_get "$CA_FD" "https://github.com/sharkdp/fd/releases/download/v7.0.0/fd_7.0.0_amd64.deb"

	f_action_install "$CA_DIODON" diodon
	f_action_install "$CA_X11VNC" x11vnc
	f_action_install "$CA_SAMBAC" "smbclient cifs-utils"
	f_action_install "$CA_GSYSLOG" gnome-system-log
	f_action_install "$CA_APTURL" apturl
	f_action_create_ppa_key_install "$CA_MULTISYSTEM" "deb http://liveusb.info/multisystem/depot all main" http://liveusb.info/multisystem/depot/multisystem.asc multisystem
	f_action_install "$CA_DROPBOX" nemo-dropbox
	f_action_ppa_install "$CA_TLP" ppa:linrunner/tlp "tlp tlp-rdw"
	f_action_ppa_install "$CA_TLP_THINKPAD" ppa:linrunner/tlp "tlp tlp-rdw tp-smapi-dkms acpi-call-tools"
	f_action_install "$CA_SYNAPTIC" synaptic

	#Installations of All PPA
	if [ "X$ALL_PPA_INSTALL" != "X" ]
	then
		echo "$NS_MARK_TITLE $NS_ALL_PPA "
		echo "$NS_INSTALL $ALL_PPA_NS"
		echo ""
		notify-send -i system-software-update "$NS_ALL_PPA" "$NS_INSTALL $ALL_PPA_NS" -t 5000
		sudo apt update
		sudo apt -y install $ALL_PPA_INSTALL 2> >(tee -a $MY_STDERR >&2)
	fi

	f_action_exec "$CA_AUTOREMOVE" "sudo apt -y autoremove && sudo apt -y autoclean"
	f_action_exec "$CA_RES_DEP" "sudo apt-get -f install"

	# Notification End 
	if [ -s $MY_STDERR ]
	then
		notify-send -i dialog-error "$NS_END_ERRORS" -t 5000
		echo "$NS_MARK_TITLE $NS_ERR_INSTALL_ALL"
		cat $MY_STDERR
	else
		notify-send -i dialog-ok "$NS_END_TITLE $NS_END_TEXT" -t 5000
	fi

	zenity --text-info --html --filename="$FN_FINAL_MSG" --height 900 --width 1200 --title "$MSG_END_TITLE"
else
	zenity --question --height 200 --width 500 --title "$MSG_END_CANCEL_TITLE" --text "$MSG_END_CANCEL_TEXT"
	if [ $? == 0 ] 
	then
		zenity --text-info --html --filename="$FN_FINAL_MSG" --height 900 --width 1200 --title "$MSG_END_TITLE"
	fi
fi

/bin/rm $FN_FINAL_MSG
