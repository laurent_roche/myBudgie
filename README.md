# README #
(En Français plus bas)

## What's this ? ##
This is a script to install applications after installation of Ubuntu Budgie 20.04 (this is the first version of Post Install Scripts for Ubuntu Budgie).  

Applications will be installed using *apt install*, or a *ppa* (plus *apt install*) or a *DEB* file (downloading the file and then using *dpkg*).

The advantages are:

 * execute only one *apt update* for all *ppa*
 * easy (point and click) interface to add extra software
 * all done in one go.

This is my installation (hence the name), you might suggest transformations/optimizations and even extra software.
But for extra applications, chances are that I am going to refuse (remember, that's my installation).
If you want your configuration, with your set of applications ... just fork the project and make your own script.

## How to install it ? ##

Download the [latest version](https://bitbucket.org/ComputingFroggy/mybudgie/get/tip.zip) and unzip the files in the directory of your choice.

From the command line, you may run:  

```
#!shell

$ wget https://bitbucket.org/ComputingFroggy/mybudgie/get/tip.zip && unzip tip.zip
```


## How to run it ? ##

Just run the file from the command line:

```
#!shell

$ ./mybudgie.sh
```
or double-click on the file *mybudgie* from *Nemo*.

The language used will picked up from your system preferences.
You may give your favourite language as a parameter (only from the command line):
```
#!shell

$ ./mybudgie.sh fr
```
At the moment, there's only two languages: French and English (which is the default, in case your language is not found).

Here is what this looks like:
![screenshot_eOS.jpg](https://bitbucket.org/repo/AAL8AL/images/397270276-screenshot_eOS.jpg)  
By default, all the applications which **I think** are mandatory are checked, it's up to you to check the other ones you want.

***Happy installation ! ;-)***

(French Version from here)

## Qu'est-ce que c'est ? ##

Ceci est un script pour installer des applications après une installation de Ubuntu Budgie 20.04 (ceci est la première version du script de Post Installation pour Ubuntu Budgie).  

Les applications seront installés en utilisant *apt install*, ou bien un *ppa* (plus *apt install*) ou un fichier *DEB* (en téléchargeant le fichier et en utilisant *dpkg*).

Les avantages sont :

 * exécution d'un seul *apt update* pour tous les *ppa*
 * interface facile (pointer et cliquer) pour ajouter des applications
 * tout est fait dans un seul lancement

Ceci est mon installation (d'où le nom), vous pouvez suggérer des transformations/optimisations et même des applications supplémentaires.
Cependant, pour les applications supplémentaires, il est possible que je refuse (rappelez-vous, c'est mon installation).
Si vous voulez votre configuration, avec votre jeux d'applications ... forkez le projet et créez votre propre script.

## Comment l'installer ? ##

Téléchargez la [dernière version](https://bitbucket.org/ComputingFroggy/mybudgie/get/tip.zip) et dézippez les fichiers dans le répertoire de votre choix.

Depuis la ligne de commande, vous pouvez taper :  

```
#!shell

$ wget https://bitbucket.org/ComputingFroggy/mybudgie/get/tip.zip && unzip tip.zip
```


## Comment l'exécuter ? ##

Juste tapez sur la ligne de commande :

```
#!shell

$ ./mybudgie.sh
```
ou double-cliquez sur le fichier *mybudgie* depuis le gestionnaire de fichiers, *Nemo*.

La langue utilisée sera prise dans vos préférences systèmes.
Vous pouvez donner votre langue favorite en paramètre (seulement depuis la ligne de commande) :
```
#!shell

$ ./mybudgie.sh fr
```
Pour le moment, il y a seulement 2 langues : Français et Anglais (qui est le défaut si votre langue n'est pas trouvée).

Pour voir à quoi ça ressemble, voir la partie en Anglais.

Par défaut, toutes les applications que je pense être obligatoire sont déjà cochées, à vous de cocher les autres si vous désirez les installer.

***Bonne installation ! ;-)***
